---
title: "جلسه ۱۵۰"
description: "زبان توصیفی QML"
date: "1397-05-22"
author: "گودرز جعفری"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster150.jpg"
---
[![poster150](../../img/posters/poster150.jpg)](../../img/poster150.jpg)

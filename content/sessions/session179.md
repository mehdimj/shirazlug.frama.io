---
title: "جلسه ۱۷۹"
description: "ارتباط Real-time با WebRTC"
date: "1398-08-01"
author: "مریم بهزادی"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster179.jpg"
---
[![poster179](../../img/posters/poster179.jpg)](../../img/poster179.jpg)

صد و هفتاد و نهمین نشست شیرازلاگ با حضور اعضای محترم در دانشگاه صنعتی شیراز برگزار شد. در این نشست مهندس رحیم فیروزی در رابطه با کاربردهای گسترده‌ی WebRTC و نحوه‌ی ارتباط real-time با این ابزار پرداختند. 

سرفصل‌هایی که در این نشست مورد بحث و بررسی قرار گرفت عبارتند از:

* مفاهیم اولیه WebRTC
* NAT Tools (ICE, TURN, STUN)
* Media Server (Kurento)
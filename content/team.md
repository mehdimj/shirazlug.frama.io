## راهبران ۹۸
<a href="/members/khozaei/" target="_blank"><img src="/img/team/khozaei.svg" alt="امین خزاعی"/></a>
<a href="/members/behzadi/" target="_blank"><img src="/img/team/behzadi.svg" alt="مریم بهزادی"/></a>
<a href="#" target="_blank"><img src="/img/team/mohajer.svg" alt="مهدی مهاجر"/></a>
<a href="#" target="_blank"><img src="/img/team/molaei.svg" alt="علی مولایی"/></a>
<a href="#" target="_blank"><img src="/img/team/firouzi.svg" alt="سید رحیم فیروزی"/></a>

---

## گرافیک
<a href="/members/behzadi/" target="_blank"><img src="/img/team/behzadi.svg" alt="مریم بهزادی"/></a>
<a href="#" target="_blank"><img src="/img/team/nezam.svg" alt="محسن نظام الملکی"/></a>
<a href="/members/khozaei/" target="_blank"><img src="/img/team/khozaei.svg" alt="امین خزاعی"/></a>

---

## توسعهٔ سایت
<a href="/members/behzadi/" target="_blank"><img src="/img/team/behzadi.svg" alt="مریم بهزادی"/></a>
<a href="/members/khozaei/" target="_blank"><img src="/img/team/khozaei.svg" alt="امین خزاعی"/></a>
<a href="#" target="_blank"><img src="/img/team/mirshaei.svg" alt="محمد میرشایی"/></a>
<a href="#" target="_blank"><img src="/img/team/nezam.svg" alt="محسن نظام الملکی"/></a>
<a href="#" target="_blank"><img src="/img/team/nikkhah.svg" alt="وجیهه نیکخواه"/></a>
<a href="#" target="_blank"><img src="/img/team/barzegar.svg" alt="پویا برزگر"/></a>
<a href="#" target="_blank"><img src="/img/team/beyzavi.svg" alt="زهره بیضاوی"/></a>
<a href="/members/razmjoo/" target="_blank"><img src="/img/team/razmjoo.svg" alt="بابک رزمجو"/></a>

---

## رادیولاگ
<a href="#" target="_blank"><img src="/img/team/mohajer.svg" alt="مهدی مهاجر"/></a>

---

## شبکه‌های اجتماعی
<a href="#" target="_blank"><img src="/img/team/nikkhah.svg" alt="وجیهه نیکخواه"/></a>
<a href="#" target="_blank"><img src="/img/team/shoaei.svg" alt="شعاعی"/></a>

---

## برنامه‌ریزی و هماهنگی جلسات
<a href="/members/behzadi/" target="_blank"><img src="/img/team/behzadi.svg" alt="مریم بهزادی"/></a>
<a href="/members/khozaei/" target="_blank"><img src="/img/team/khozaei.svg" alt="امین خزاعی"/></a>
<a href="#" target="_blank"><img src="/img/team/shoaei.svg" alt="شعاعی"/></a>

<!-- TODO We need a method to automatically populate these lists -->
